﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface IMyDbInitializer
    {
        public void Init();
    }
}